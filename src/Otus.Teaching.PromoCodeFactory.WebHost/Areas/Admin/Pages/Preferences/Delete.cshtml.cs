using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Areas.Admin.Pages.Preferences
{
    public class DeleteModel : PageModel
    {
        private readonly IRepository<Preference> _repository;

        public DeleteModel(IRepository<Preference> repository)
        {
            _repository = repository;
        }

        public async Task<IActionResult> OnGetAsync(Guid id)
        {
            var toDelete = await _repository.GetByIdAsync(id);
            if (toDelete != null)
            {
                await _repository.DeleteAsync(toDelete);
            }

            return RedirectToPage("/Preferences/Index");
        }
    }
}
