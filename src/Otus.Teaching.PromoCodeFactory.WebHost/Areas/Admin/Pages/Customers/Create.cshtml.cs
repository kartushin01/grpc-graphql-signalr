using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Dto;
using Otus.Teaching.PromoCodeFactory.WebHost.Mappers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Areas.Admin.Pages.Customers
{
    public class CreateModel : PageModel
    {

        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;

        public CreateModel(IRepository<Customer> customerRepository, IRepository<Preference> preferenceRepository)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
        }

        public string PageTitle { get; set; }

        [ModelBinder]
        public CustomerCreateOrEditDto CustomerModel { get; set; }
        public async Task<IActionResult> OnGet()
        {
            PageTitle = "�������� �������";

            CustomerModel = new CustomerCreateOrEditDto();
          
            var preferences = await _preferenceRepository.GetAllAsync();

            CustomerModel.Preferences = preferences.Select(c => new SelectListItem() { Text = c.Name, Value = c.Id.ToString() }).ToList();

            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            var createdCustomer = new Customer()
            {
                Id = Guid.NewGuid(),
                LastName = CustomerModel.LastName,
                FirstName = CustomerModel.FirstName,
                Email = CustomerModel.Email
            };

            var preferences = await _preferenceRepository.GetAllAsync();

            createdCustomer.Preferences = CustomerModel.PreferencesIds.Select(
                x => new CustomerPreference()
                {
                    PreferenceId = x, CustomerId = createdCustomer.Id, Customer = createdCustomer, Preference = preferences.FirstOrDefault(c=>c.Id == x)
                }).ToList();

            try
            {
                await _customerRepository.AddAsync(createdCustomer);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

            return RedirectToPage("./Index");
        }
    }
}
