using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Areas.Admin.Pages.Customers
{
    public class IndexModel : PageModel
    {
        private readonly IRepository<Customer> _customerRepository;

        public IndexModel(IRepository<Customer> customerRepository)
        {
            _customerRepository = customerRepository;
        }

        public string PageTitle { get; set; }
        public List<CustomerShortResponse> Customers { get; set; }

        public async Task<IActionResult> OnGetAsync()
        {
            PageTitle = "������ ��������";

            var customers = await _customerRepository.GetAllAsync();
            
            Customers = customers.Select(x => new CustomerShortResponse()
            {
                Id = x.Id,
                LastName = x.LastName,
                FirstName = x.FirstName,
                Email = x.Email
            }).ToList();

            return Page();
        }
    }
}
