﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HotChocolate;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.GraphQL
{
    public class Mutation
    {

        public async Task<Customer> CreateCustomer(AddCustomerInput input, [Service] DataContext context)
        {
            Customer newCastomer = new Customer();
            
            newCastomer.Id = Guid.NewGuid();
            newCastomer.LastName = input.LastName;
            newCastomer.FirstName = input.FirstName;
            newCastomer.Email = input.FirstName;
           
            var preferences = new List<CustomerPreference>();
            
            for (int i = 0; i < input.PreferencesIds.Length; i++)
            {
                Preference pref = await context.Preferences.FirstOrDefaultAsync(x => x.Id == Guid.Parse(input.PreferencesIds[i]));

                preferences.Add(new CustomerPreference()
                {
                    CustomerId = newCastomer.Id,
                    PreferenceId = Guid.Parse(input.PreferencesIds[i]),
                    Preference = pref

                });
            }

            newCastomer.Preferences = preferences.ToList();

            await context.Customers.AddAsync(newCastomer);
            await context.SaveChangesAsync();
            return newCastomer;
        }

        public async Task<Preference> CreatePreference(AddPreferenceInput input,
            [Service] DataContext context)
        {
            Preference newPreference = new Preference()
            {   
                Id = Guid.NewGuid(),
                Name = input.Name
            };
            await context.Preferences.AddAsync(newPreference);
            await context.SaveChangesAsync();
            return newPreference;
        }
    }
}
