﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Mappers
{
    public static class PreferenceMapper
    {
        public static PreferenceResponse MappingFromDb(Preference preference)
        {
            PreferenceResponse model = new PreferenceResponse();
            model.Id = preference.Id;
            model.Name = preference.Name;
            return model;
        }

        public static Preference MappinfFromModel(PreferenceResponse model, Preference preference = null)
        {
            if (preference == null)
            {
                preference = new Preference();
                preference.Id = Guid.NewGuid();
            }

            preference.Name = model.Name;

            return preference;
        }
    }
}
