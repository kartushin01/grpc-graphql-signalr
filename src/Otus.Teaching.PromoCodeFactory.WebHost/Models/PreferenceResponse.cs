﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class PreferenceResponse
    {
        public PreferenceResponse()
        {
            
        }

        public PreferenceResponse(string id, string name)
        {
         Id = Guid.Parse(id);
         Name = name;
        }

        public Guid Id { get; set; }
        
        [Required]
        public string Name { get; set; }
    }
}