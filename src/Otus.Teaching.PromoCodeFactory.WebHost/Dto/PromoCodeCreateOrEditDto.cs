﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Dto
{
    public class PromoCodeCreateOrEditDto
    {
        public Guid Id { get; set; }
        [Required, Display(Name = "Код")]
        public string Code { get; set; }
        [Required, Display(Name = "Инфо")]
        public string ServiceInfo { get; set; }
        [Required, Display(Name = "Начало действия")]
        public DateTime BeginDate { get; set; }
        [Required, Display(Name = "Окончание действия")]
        public DateTime EndDate { get; set; }
        public List<SelectListItem> Partners { get; set; }
        public List<SelectListItem> Preferences { get; set; }
        [Required, Display(Name = "Предпочтение")]
        public Guid PreferenceId { get; set; }
        [Required, Display(Name = "Партнер")]
        public Guid PartnerId { get; set; }


    }
}
